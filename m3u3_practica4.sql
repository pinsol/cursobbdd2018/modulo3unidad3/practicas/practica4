﻿/*                              Practica 4                                          */
DROP DATABASE practica4apoyo;
CREATE DATABASE practica4apoyo;
USE practica4apoyo;

/* Ejercicio 1: Crea un procedimiento que reciba una cadena que puede contener letras y números
                y sustituya los números por *. El argumento debe ser único y almacenar en él, el resultado */

DELIMITER //
CREATE OR REPLACE PROCEDURE ejerc4_1(INOUT cadena varchar(20))
BEGIN

DECLARE ini int DEFAULT 0;
DECLARE fin int DEFAULT 9;
DECLARE cont int DEFAULT ini;


WHILE (cont<=fin) DO
  set cadena = REPLACE(cadena,cont,'*');
  set cont=cont+1;
END WHILE;

END //
DELIMITER ;

set @a="ejem32 ";
CALL ejerc4_1(@a);
SELECT @a;

/* Ejercicio 2: Crea una función que recibe como argumento una cadena que puede contener letras y números
                y debe devolver la cadena con el primer carácter en mayúsculas y el resto en minúculas */

DELIMITER //
CREATE OR replace FUNCTION ejerc4_2(cadena varchar(25))
  RETURNS varchar(25)
  BEGIN
  RETURN (SELECT CONCAT(UPPER(SUBSTR(cadena,1,1)),SUBSTR(cadena,2)));
  END //
DELIMITER ;

SELECT ejerc4_2('piter');


/* Ejercicio 3: Desarrollar una función que devuelva el número de años completos que hay entre dos fechas
                que se pasan como parámetros. Utiliza la función DATEDIFF */

DELIMITER //
CREATE OR REPLACE FUNCTION ejer4_3(fecha1 date, fecha2 date)
  RETURNS int
  BEGIN
  RETURN (SELECT (DATEDIFF(fecha1, fecha2))/365);
  END //
DELIMITER ;

SELECT ejer4_3('2017-02-17','2015-03-25');